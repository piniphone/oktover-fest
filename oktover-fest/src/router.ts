import { createRouter, createWebHistory } from "vue-router";

import BeersModuleRoutes from "@/modules/beers";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      children: BeersModuleRoutes,
    },
  ],
});

export default router;
