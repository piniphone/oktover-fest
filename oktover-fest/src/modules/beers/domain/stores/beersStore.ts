import { defineStore } from "pinia";
import { computed, reactive, ref } from "vue";
import type { Beer, BeerList } from "../entities/Beer";
import { useApiSearch } from "@/modules/common/composables/useApiSearch";
import type { BeerSearchParams } from "../entities/BeerSearchParams";
import { useApiCrud } from "@/modules/common/composables/useApiCrud";
import type { ApiId } from "@/modules/common/domain/entities/ApiId";

export const useBeersStore = defineStore("beers", () => {
  const searchParams = reactive<BeerSearchParams>({});
  const beersApi = useApiSearch<BeerList, BeerSearchParams>(
    "beers",
    searchParams
  );
  const beersApiCruds = useApiCrud<Beer>("beers");
  const results = ref<BeerList[]>([]);
  const result = ref<Beer | null>();

  async function search() {
    results.value = await beersApi.search();
  }

  async function get(id: ApiId) {
    result.value = await beersApiCruds.get(id);
  }

  async function resetResult() {
    result.value = null;
  }

  async function searchNext() {
    results.value = await beersApi.next();
  }

  async function searchPrev() {
    results.value = await beersApi.prev();
  }

  const canNavNext = computed(() => {
    return results.value.length === beersApi.pagination.params.per_page;
  });

  return {
    state: {
      loading: beersApi.loading,
      results,
      resultLoading: beersApiCruds.loading,
      result,
      searchParams,
      canNavNext,
    },
    pagination: beersApi.pagination,
    search,
    searchNext,
    searchPrev,
    get,
    resetResult,
  };
});
