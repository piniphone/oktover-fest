export type UnitType = "celsius" | "grams" | "kilograms" | "litres";

export interface BeerUnitValue {
  value: number;
  unit: UnitType;
}

export interface BeerMashTemp {
  temp: BeerUnitValue;
  duration?: number;
}

export interface BeerFermentation {
  temp: BeerUnitValue;
}

export interface BeerMethod {
  mash_temp: BeerMashTemp[];
  fermentation: BeerFermentation;
  twist: string;
}

export interface BeerBasicIngredient {
  name: string;
  amount: BeerUnitValue;
}

export interface BeerHopIngredient extends BeerBasicIngredient {
  add: string;
  attribute: string;
}

export interface BeerIngredients {
  malt: BeerBasicIngredient[];
  hops: BeerHopIngredient[];
  yeast: string;
}

export interface BeerList {
  id: number;
  name: string;
  tagline: string;
  first_brewed: string;
  description: string;
  image_url: string;
}
export interface Beer extends BeerList {
  abv: number;
  ibu?: number;
  target_fg: number;
  target_og: number;
  ebc?: number;
  srm?: number;
  ph?: number;
  attenuation_level: number;
  volume: BeerUnitValue;
  boil_volume: BeerUnitValue;
  method: BeerMethod;
  ingredients: BeerIngredients;
  food_pairing: string[];
  brewers_tips: string;
  contributed_by: string;
}
