import type { RouteRecordRaw } from "vue-router";
import BeersView from "./ui/views/BeersView.vue";
import BeerView from "./ui/views/BeerView.vue";

const moduleRoutes: RouteRecordRaw[] = [
  {
    path: "/",
    name: "beers",
    component: BeersView,
    children: [
      {
        path: "/:id",
        name: "beer",
        component: BeerView,
      }
    ]
  },
];

export default moduleRoutes;
