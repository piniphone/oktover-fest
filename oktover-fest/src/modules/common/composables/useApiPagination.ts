import { reactive } from "vue";

interface ApiPaginationParams {
  page: number;
  per_page: number;
};

const INITIAL_PARAMS: ApiPaginationParams = {
  page: 1,
  per_page: 20,
};

export function useApiPagination() {
  const params = reactive<ApiPaginationParams>({ ...INITIAL_PARAMS });

  const next = () => params.page++;
  const prev = () => params.page--;

  const reset = () => {
    params.page = INITIAL_PARAMS.page;
    params.per_page = INITIAL_PARAMS.per_page;
  };

  return {
    params,
    next,
    prev,
    reset,
  };

};
