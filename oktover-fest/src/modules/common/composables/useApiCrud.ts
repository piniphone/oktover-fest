import { ref } from "vue";
import { api } from "../api";
import type { ApiId } from "../domain/entities/ApiId";

export function useApiCrud<T>(endpoint: string) {
  const loading = ref<boolean>(false);

  const get = async function (id: ApiId): Promise<T> {
    try {
      loading.value = true;
      const result = await api.get<T[]>([endpoint, id].join("/"));
      return result.data[0];
    } finally {
      loading.value = false;
    }
  };

  const save = async function (data: Partial<T>): Promise<void> {
    await api.post(endpoint, data);
  };

  const remove = async function (id: ApiId): Promise<void> {
    await api.delete([endpoint, id].join("/"));
  };

  return {
    loading,
    save,
    get,
    remove,
  };
}
