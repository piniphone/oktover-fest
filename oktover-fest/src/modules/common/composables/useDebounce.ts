export function useDebounce(fn: Function, time: number = 400) {
  let timeout: ReturnType<typeof setTimeout> | number;
  return (ev: any) => {
    if (timeout != null) clearTimeout(timeout);
    timeout = setTimeout(() => fn(ev), time);
  };
}
