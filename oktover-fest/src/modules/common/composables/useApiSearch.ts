import { ref } from "vue";
import { api } from "../api";
import { useApiPagination } from "./useApiPagination";

export function useApiSearch<T, K>(endpoint: string, searchParams: K) {
  const pagination = useApiPagination();
  const loading = ref<boolean>(false);

  async function search(): Promise<T[]> {
    try {
      loading.value = true;
      await new Promise(r => setTimeout(r, 500)); // la relentizo un poco
      const result = await api.get<T[]>(endpoint, {
        params: {
          ...searchParams,
          ...pagination.params,
        },
      });
      return result.data;
    } finally {
      loading.value = false;
    }
  }

  async function next(): Promise<T[]> {
    pagination.next();
    return search();
  }

  async function prev(): Promise<T[]> {
    pagination.prev();
    return search();
  }

  return {
    loading,
    pagination,
    search,
    next,
    prev,
  };
}
