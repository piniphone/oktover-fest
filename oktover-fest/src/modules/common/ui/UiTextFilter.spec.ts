import { describe, it, expect, beforeAll } from "vitest";

import { mount, VueWrapper } from "@vue/test-utils";
import UiTextFilter from "./UiTextFilter.vue";

describe("UiTextFilter", () => {
  let wrapper: any;

  beforeAll(() => {
    wrapper = mount(UiTextFilter, {
      props: {
        value: "beer test",
        debounce: 100,
        loading: false,
      },
    });
  });

  it("value is correctly passed to input value", () => {
    const inputElement = wrapper.find("input");
    expect(inputElement.attributes("type")).toBe("text");
    expect(inputElement.attributes("value")).toBe("beer test");
  });

});
